from django.urls import path, include
from rest_framework.routers import DefaultRouter

from apps.products_v1.views import AdminViewset, BrandViewSet, ProductViewSet, Statistics

app_name = 'products_v1'

router = DefaultRouter()
router.register(r'admins', AdminViewset, basename='admins')
router.register(r'brands', BrandViewSet, basename='brands')
router.register(r'products', ProductViewSet, basename='products')
router.register(r'statistics', Statistics, basename='statistics')

urlpatterns = [
    path('', include(router.urls))
]