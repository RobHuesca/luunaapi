
from rest_framework.authtoken.models import Token
from rest_framework.test import APITestCase, APIClient

from apps.products_v1.models import User


class TestSetUp(APITestCase):
    def setUp(self):
        self.product = {
            "name": "product beta",
            "sku": "00034DFT",
            "price": "0.90",
            "brand": "1"
        }
        self.admins_url = '/v1/admins/'
        self.admins_url_id = '/v1/admins/1/'
        self.brands_url = '/v1/brands/'
        self.products_url = '/v1/products/'
        self.user = User.objects.create_user(username='admin', password='demo2021')
        self.token = Token.objects.create(user=self.user)
        self.api_authentication()

    def api_authentication(self):
        self.client.credentials(HTTP_AUTHORIZATION='Token ' + self.token.key)





