from rest_framework.test import APIRequestFactory

from .test_setup import TestSetUp


class TestAdmins(TestSetUp):
    def test_get_admins(self):
        response = self.client.get(self.admins_url)
        self.assertEqual(response.status_code, 200)

    def test_admin_retrieve(self):
        response = self.client.get(self.admins_url_id)
        self.assertEqual(response.status_code, 200)

    def test_admin_update(self):
        response = self.client.put(self.admins_url_id, {"first_name": "Robie"})
        self.assertEqual(response.status_code, 202)

    def test_brands_post(self):
        response = self.client.post(self.brands_url, {'name': 'new idea'})
        self.assertEqual(response.status_code, 201)

    def test_brands_get(self):
        response = self.client.get(self.brands_url)
        self.assertEqual(response.status_code, 200)

    def test_products_post(self):
        response = self.client.post(self.brands_url, self.product)
        import pdb
        pdb.set_trace()
        self.assertEqual(response.status_code, 201)




