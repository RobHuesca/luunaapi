import datetime

from django.db import transaction

# Create your views here.
from rest_framework import viewsets, status
from rest_framework.permissions import AllowAny, IsAuthenticatedOrReadOnly
from rest_framework.response import Response

from apps.products_v1.notifications_email import send_mail_cli
from .models import Product
from apps.products_v1.models import User, Brand, ProductView
from apps.products_v1.serializers import AdminCreateSerializer, BrandSerializer, ProductSerializer, ProductViewSerializer


class AdminViewset(viewsets.ModelViewSet):
    """ Administration of user type Admin
        /* Endpoints */
        POST /v1/admins/ - Send all the params **
        GET /v1/admins/:id - Get all or by id **
        PUT /v1/admins/:id  - Update can be partial by params **
        DELETE /v1/admins/:id - Send only id **

        /* Params */
        @username:string
        @email:string
        @first_name:string
        @last_name:string
        @password:string
        @password_confirmation:string
        """
    queryset = User.objects.all().order_by('pk')
    serializer_class = AdminCreateSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, AllowAny,)
    http_method_names = ['get', 'post', 'put', 'delete']

    @transaction.atomic()
    def update(self, request, pk=None):
        user = User.objects.get(pk=pk)
        serializer = self.serializer_class(instance=user, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.save()
        data = serializer.data
        return Response(data=data, status=status.HTTP_202_ACCEPTED)


class BrandViewSet(viewsets.ModelViewSet):
    """ Catalog of brands to products
        /* Endpoints */
        POST /v1/brands/ - Send only name **
        GET /v1/admins/:id - Get all or by id **
        PUT /v1/admins/:id  - Update name **
        DELETE /v1/admins/:id - Send only id **

        /* Params */
        @name:string
        """
    queryset = Brand.objects.all()
    serializer_class = BrandSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, AllowAny,)
    http_method_names = ['get', 'post', 'put', 'delete']


class ProductViewSet(viewsets.ModelViewSet):
    """ Stock of products
        /* Endpoints */
        POST /v1/products_v1/ - Send all params **
        GET /v1/products_v1/:id - Get all or by id **
        PUT /v1/products_v1/:id  - Update can be partial by params **
        DELETE /v1/products_v1/:id - Send only id **

        /* Params */
        @name:string
        @sku:string
        @price:float
        @brand:integer - Get the id of brand catalog
    """
    queryset = Product.objects.all()
    serializer_class = ProductSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, AllowAny,)
    http_method_names = ['get', 'post', 'put', 'delete']

    @transaction.atomic()
    def update(self, request, pk=None):
        product = Product.objects.get(pk=pk)
        serializer = self.serializer_class(instance=product, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)

        """ Function to send notifications to admins """
        admin = self.request.user
        send_mail_cli(admin, self.get_object(), request.data)

        serializer.save()
        data = serializer.data
        return Response(data=data, status=status.HTTP_202_ACCEPTED)

    def retrieve(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance)
        """ Do increase of product views """

        if self.request.user.is_anonymous:
            if not ProductView.objects.filter(created_at=datetime.datetime.now()).exists():
                ProductView.objects.create(product=self.get_object(), views=1)
            else:
                product = ProductView.objects.get(product=self.get_object(), created_at=datetime.datetime.now())
                product.views = product.views + 1
                product.save()
        return Response(serializer.data)


class Statistics(viewsets.ModelViewSet):
    """
        Service to obtain statistics of the day's visit by product
        /* Endpoints */
        GET /v1/statistics/ - Get all the statistics of the day's visit by product **
    """
    http_method_names = ['get']
    queryset = ProductView.objects.all()
    serializer_class = ProductViewSerializer
    permission_classes = (IsAuthenticatedOrReadOnly, AllowAny,)


