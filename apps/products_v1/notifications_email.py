import json

import boto3

from apps.products_v1.models import User


def send_mail_cli(admin, product, data):
    """
    Receive parameters:
    ** Admin that update the product
    ** Product description
    ** Product modification
    :return:
    """
    values = json.dumps(data)

    admin_emails = User.objects.values_list('email', flat=True)
    emails = list(admin_emails)

    # Generate the AWS SES instance
    client = boto3.client('ses')
    response = client.send_email(
        Destination={
            'ToAddresses': emails,
        },
        Message={
            'Body': {
                'Html': {
                    'Charset': 'UTF-8',
                    'Data': '<h1>Hello from Luuna API</h1><p>This is a notification</p>'
                            '<p>The administrator {} {} modified: {} of the product: {}</p>'.format(
                        admin.first_name, admin.last_name, values, product.name),
                }
            },
            'Subject': {
                'Charset': 'UTF-8',
                'Data': 'Products Update',
            },
        },
        Source='roberthhuesca95@gmail.com',
    )