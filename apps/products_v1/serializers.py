from django.contrib.auth import password_validation
from django.db import transaction
from rest_framework import serializers
import boto3
from django.contrib.auth.validators import UnicodeUsernameValidator

from apps.products_v1.models import User, Brand, Product, ProductView


class AdminCreateSerializer(serializers.ModelSerializer):
    username = serializers.CharField(validators=[UnicodeUsernameValidator])
    password_confirmation = serializers.CharField(min_length=8, max_length=20, required=False)

    class Meta:
        model = User
        fields = ['pk', 'username', 'email', 'first_name', 'last_name', 'password', 'password_confirmation']
        read_only_fields = ['pk']
        extra_kwargs = {'password': {'write_only': True}}

    def validate(self, data):
        if not self.instance:
            """ Validate passwords and email when is a create """
            password = data['password']
            password_confirmation = data['password_confirmation']
            if password != password_confirmation:
                raise serializers.ValidationError('The passwords entered do not match')
            password_validation.validate_password(password)
            if User.objects.filter(email__iexact=data['email']).exists():
                raise serializers.ValidationError("The Email entered already exists")
        return data

    @transaction.atomic()
    def create(self, validated_data):
        validated_data.pop('password_confirmation')
        instance = User(**validated_data)
        instance.save()

        """ Send email to admin to verify the email account in AWS SES  """
        ses = boto3.client('ses')
        response = ses.verify_email_identity(
            EmailAddress=validated_data['email']
        )
        return instance

    def update(self, instance, validated_data):
        instance.email = validated_data.get('email', instance.email)
        instance.first_name = validated_data.get('first_name', instance.first_name)
        instance.last_name = validated_data.get('last_name', instance.last_name)
        instance.password = validated_data.get('password', instance.password)
        instance.username = validated_data.get('username', instance.username)
        instance.save()
        return instance


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = ['pk', 'name']

    @transaction.atomic()
    def create(self, validated_data):
        instance = Brand(**validated_data)
        instance.created_by = self.context['request'].user
        instance.save()
        return instance


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ['pk', 'name', 'sku', 'price', 'brand']

    @transaction.atomic()
    def create(self, validated_data):
        instance = Product(**validated_data)
        instance.created_by = self.context['request'].user
        instance.save()
        return instance

    def update(self, instance, validated_data):
        instance.name = validated_data.get('name', instance.name)
        instance.sku = validated_data.get('sku', instance.sku)
        instance.price = validated_data.get('price', instance.price)
        instance.brand = validated_data.get('brand', instance.brand)
        instance.save()
        return instance

    def validate(self, data):
        if not self.instance:
            if Product.objects.filter(sku=data['sku']).exists():
                raise serializers.ValidationError("The SKU entered already exists")
        return data


class ProductViewSerializer(serializers.ModelSerializer):
    product = ProductSerializer()

    class Meta:
        model = ProductView
        fields = ['created_at', 'views', 'product']
