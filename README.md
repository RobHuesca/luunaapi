# Luuna API v1 - Backend Technical Test
By Roberto Huesca

Is a API developed with technologies like Python, Django Framework, PostgreSQL
and use the next libraries:
- Oauth2 for the security API
- DjangoRestFramework for the develop of services
- Corsheaders for the petitions to services
- Boto3 is the AWS SDK for python to interaction with the emails notification
- RestFrameworkSwagger for the automatically documentation of services


## Installation

Use the package manager pip to install the libraries.

```bash
pip install -r requirements.txt
```
Create a file name secret.json where is the configuration of DB
```bash
    "SECRET_KEY": "",
    "DEBUG": 0,
    "DB_NAME":"",
    "DB_USER": "",
    "DB_PASSWORD": "",
    "DB_HOST": "",
    "DB_PORT": ""
```
Execute the next command to generate the DB
```python
   python manage.py migrate
```
## Usage
The urls of api utilize the methods ['GET', 'POST', 'PUT', 'DELETE']:
- /products/
- /brands/
- /admins/